package org.vgiscience.orfinder.structures;

public class QueryStat {

	// amount of queries
	private int countNoMethod;
	private int countPointOnLE;
	private int countBounds;
	private int countIsOptimal;

	private long dijkstraTime;

	public QueryStat() {
		this(0, 0, 0, 0, 0);
	}

	public QueryStat(int countNoMethod, int countPointOnLE, int countBounds, int countIsOptimal, long dijkstraTime) {
		this.countNoMethod = countNoMethod;
		this.countPointOnLE = countPointOnLE;
		this.countBounds = countBounds;
		this.countIsOptimal = countIsOptimal;
		this.dijkstraTime = dijkstraTime;
	}

	public int getCountNoMethod() {
		return countNoMethod;
	}

	public int getCountPointOnLE() {
		return countPointOnLE;
	}

	public int getCountBounds() {
		return countBounds;
	}

	public int getCountIsOptimal() {
		return countIsOptimal;
	}

	public int getTotalCount() {
		return countNoMethod + countPointOnLE + countBounds + countIsOptimal;
	}

	public void addDijkstraTime(long milliseconds) {
		this.dijkstraTime += milliseconds;
	}

	public long getDijkstraTime() {
		return dijkstraTime;
	}

	public void calculatedLineAtLowerEnvelopeIn(QueryType type) {
		switch (type) {
		case findOptimalRangeBound:
			countBounds++;
			break;
		case findPointOnLowerEnvelope:
			countPointOnLE++;
			break;
		case isOptimal:
			countIsOptimal++;
			break;
		case noMethod:
			countNoMethod++;
			break;
		}
	}

	@Override
	public String toString() {
		return "QueryStat [countNoMethod=" + countNoMethod + ", countPointOnLE=" + countPointOnLE + ", countBounds="
				+ countBounds + ", countIsOptimal=" + countIsOptimal + "]";
	}
}
