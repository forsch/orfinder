package org.vgiscience.orfinder.structures;

import java.util.Iterator;
import java.util.Objects;

public class Interval implements Comparable<Interval> {
	private double a;
	private double b;
	private boolean aInclusive;
	private boolean bInclusive;

	public Interval(double a, boolean aInclusive, double b, boolean bInclusive) {
		setUpperBound(b);
		setLowerBound(a);
		this.aInclusive = aInclusive;
		this.bInclusive = bInclusive;
	}

	public Interval(double a, double b) {
		this(a, true, b, true);
	}

	public Interval(double[] rn) {
		this(rn[0], rn[1]);
	}

	public Interval() {
		this(0, 1);
	}

	public Interval(Interval copy) {
		this(copy.a, copy.aInclusive, copy.b, copy.bInclusive);
	}

	public double getLowerBound() {
		return a;
	}

	public void setLowerBound(double a) {
		if (b < a)
			throw new IllegalArgumentException("a must be smaller or equal to b, got: a=" + a + ", b=" + b);
		if (a < 0)
			throw new IllegalArgumentException("a cannot be smaller than zero, got: a=" + a);
		this.a = a;
	}

	public double getUpperBound() {
		return b;
	}

	public boolean lowerBoundInclusive() {
		return aInclusive;
	}

	public boolean upperBoundInclusive() {
		return bInclusive;
	}

	public void setUpperBound(double b) {
		if (b < a)
			throw new IllegalArgumentException("a must be smaller or equal to b, got: a=" + a + ", b=" + b);
		if (b > 1)
			throw new IllegalArgumentException("b cannot be bigger than one, got: b=" + b);
		this.b = b;
	}

	public static Interval intersect(Iterable<Interval> intervals) {
		Interval ret = new Interval(new double[] { 0, 1 });
		Iterator<Interval> it = intervals.iterator();
		while (ret != null && it.hasNext()) {
			ret = ret.intersect(it.next());
		}
		return ret;
	}

	public Interval intersect(Interval o) {
		Objects.requireNonNull(o);
		if (!intersects(o))
			return null;

		double newA, newB;
		boolean aincl, bincl;
		if (a - o.a > 0) {
			newA = a;
			aincl = aInclusive;
		} else {
			newA = o.a;
			aincl = o.aInclusive;
		}
		if (b - o.b < 0) {
			newB = b;
			bincl = bInclusive;
		} else {
			newB = o.b;
			bincl = o.bInclusive;
		}
		return new Interval(newA, aincl, newB, bincl);
	}

	public boolean intersects(Interval o) {
		if (b == o.a && (!bInclusive || !o.aInclusive))
			return false;
		if (a == o.b && (!aInclusive || !o.bInclusive))
			return false;
		if (b < o.a || a > o.b)
			return false;
		return true;
	}

	public double size() {
		return b - a;
	}

	public boolean contains(Interval o) {
		return contains(o.a) && contains(o.b);
	}

	@Override
	public String toString() {
		return (aInclusive ? "[" : "(") + String.format("%5.3f;%5.3f", a, b) + (bInclusive ? "]" : ")");
	}

	public boolean contains(double rationalNumber) {
		if (a < rationalNumber || (a == rationalNumber && aInclusive)) {
			return b > rationalNumber || (b == rationalNumber && bInclusive);
		}
		return false;
	}

	@Override
	public int compareTo(Interval o) {
		if (a == o.a) {
			if (aInclusive && !o.aInclusive)
				return -1;
			if (!aInclusive && o.aInclusive)
				return 1;
			return Double.compare(b, o.b);
		}
		return Double.compare(a, o.a);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Interval))
			return false;
		Interval o = (Interval) obj;
		if (aInclusive == o.aInclusive && a == o.a && bInclusive == o.bInclusive && b == o.b)
			return true;
		return false;
	}
}
