package org.vgiscience.orfinder.structures;

public enum QueryType {
	noMethod, findPointOnLowerEnvelope, findOptimalRangeBound, isOptimal
}
