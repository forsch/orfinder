package org.vgiscience.orfinder;

import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.logging.Logger;

import org.vgiscience.orfinder.exceptions.AlphaOutsideIntervalException;
import org.vgiscience.orfinder.exceptions.BoundaryLineNotOptimalException;
import org.vgiscience.orfinder.exceptions.InvalidIntervalException;
import org.vgiscience.orfinder.exceptions.MaximumIterationException;
import org.vgiscience.orfinder.linearrangement.LongLine;
import org.vgiscience.orfinder.linearrangement.LongLineProvider;
import org.vgiscience.orfinder.structures.Interval;
import org.vgiscience.orfinder.structures.QueryStat;
import org.vgiscience.orfinder.structures.QueryType;

public class LowerEnvelopeFinder<T> {
	private static Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	// constants for easier readability of the code
	private static final int QUERY_LINE_IS_LOWER = -1;
	private static final int LINES_ARE_EQUAL = 0;
	private static final int BOUNDARY_LINE_IS_LOWER = 1;

	private double[] bounds;
	private LongLineProvider<T> lp;

	private static int MAXIMUM_ITERATION = 9999;

	public LowerEnvelopeFinder(LongLineProvider<T> lp) {
		this.bounds = new double[2];
		this.lp = lp;
	}

	/**
	 * Searches an (arbitrary) alpha value for which the current query path is
	 * optimal. An optional interval can be given to limit the search space in case
	 * of previous knowledge.
	 * 
	 * @param givenBounds (optional) initial bounds to limit search interval
	 * @param sw          stop watch for timing of running time
	 * @param stat        collector for run statistics
	 * @return (arbitrary) alpha value for which query line is optimal or -1 if line
	 *         is not on lower envelope
	 * @throws MaximumIterationException
	 */
	public double findPointOnLowerEnvelope(Interval givenBounds, Optional<QueryStat> stat)
			throws MaximumIterationException {
		bounds[0] = givenBounds.getLowerBound();
		bounds[1] = givenBounds.getUpperBound();
		double alpha;

		LongLine<T> pi = lp.getQueryLine();

		int iteration = 0;
		while (true) {
			alpha = (bounds[0] + bounds[1]) / 2;

			// get line on lower envelope for given alpha
			LongLine<T> p = lp.getLineAtLowerEnvelope(alpha, stat);
			stat.ifPresent(s -> s.calculatedLineAtLowerEnvelopeIn(QueryType.findPointOnLowerEnvelope));

			// found value is same as value of line
			if (p.compareValueTo(pi, alpha) == 0)
				return alpha;

			// given line is dominated by p --> line pi is not on lower envelope
			if (p.getPrimary() < pi.getPrimary() && p.getSecondary() < pi.getSecondary())
				return -1;

			// calculate crossing point of p and pi
			alpha = (double) (p.getPrimary() - pi.getPrimary())
					/ (pi.getSecondary() - pi.getPrimary() - p.getSecondary() + p.getPrimary());

			// crossing point is outside of valid interval
			if (alpha < givenBounds.getLowerBound() || alpha > givenBounds.getUpperBound())
				return -1;

			if (pi.getPrimary() > p.getPrimary())
				bounds[0] = alpha;
			else
				bounds[1] = alpha;

			// af new change 26.08.2021 to avoid endless loops, must be checked!
			if (bounds[0] > bounds[1])
				return -1;

			// avoid endless loops
			iteration++;
			if (iteration >= MAXIMUM_ITERATION)
				throw new MaximumIterationException(
						"Maximum Iteration reached without result in findPointOnLowerEnvelope!", MAXIMUM_ITERATION);

		}
	}

	/**
	 * See {@link #findPointOnLowerEnvelope(Interval, QueryStat)}
	 * 
	 * @param sw   stop watch for timing of running time
	 * @param stat collector for run statistics
	 * @return (arbitrary) alpha value for which query line is optimal
	 * @throws MaximumIterationException
	 */
	public double findPointOnLowerEnvelope(Optional<QueryStat> stat) throws MaximumIterationException {
		return findPointOnLowerEnvelope(new Interval(0, 1), stat);
	}

	/**
	 * Given an (arbitrary) alpha value inside the optimality interval, this method
	 * computes the lower OR upper boundary of the optimality interval.
	 * 
	 * @param alpha_inside     (arbitrary) alpha value inside the optimality
	 *                         interval
	 * @param searchLowerBound if true, lower boundary is searched, else upper
	 *                         boundary
	 * @param sw               stop watch for timing of running time
	 * @param stat             collector for run statistics
	 * @return
	 * @throws MaximumIterationException
	 * @throws BoundaryLineNotOptimalException
	 * @throws AlphaOutsideIntervalException
	 */
	private double findOptimalRangeBound(double alpha_inside, boolean searchLowerBound, Optional<QueryStat> stat)
			throws MaximumIterationException, BoundaryLineNotOptimalException, AlphaOutsideIntervalException {
		double relevantBound = bounds[1];
		if (searchLowerBound)
			relevantBound = bounds[0];

		double[] searchInterval = { relevantBound, alpha_inside };

		LongLine<T> queryLine = lp.getQueryLine();
		LongLine<T> boundaryLine = lp.getLineAtLowerEnvelope(relevantBound, stat);
		stat.ifPresent(s -> s.calculatedLineAtLowerEnvelopeIn(QueryType.findOptimalRangeBound));

		// query line is also on the lower boundary at the relevantBound (crossing point
		// with boundaryLine) --> should actually never happen because of optimality
		// checks in "findBounds(..)"
		if (queryLine.compareValueTo(boundaryLine, relevantBound) == 0)
			return relevantBound;

		double alphaCrossing, alphaMid;
		int iteration = 0;
		int comparisonResult;
		while (true) {
			alphaMid = (searchInterval[0] + searchInterval[1]) / 2;
			boundaryLine = lp.getLineAtLowerEnvelope(alphaMid, stat);

			comparisonResult = queryLine.compareValueTo(boundaryLine, alphaMid);

			switch (comparisonResult) {

			case LINES_ARE_EQUAL:
				if (queryLine.getPrimary() > boundaryLine.getPrimary())
					return alphaMid;

				searchInterval[1] = alphaMid;
				break;

			case QUERY_LINE_IS_LOWER:
				throw new BoundaryLineNotOptimalException("query Line (" + queryLine + ") is lower for "
						+ String.format("%.3f", alphaMid) + " -> Should never happen, as boundary line (" + boundaryLine
						+ ") should be optimal!");

			case BOUNDARY_LINE_IS_LOWER:

				alphaCrossing = queryLine.getAlphaCrossing(boundaryLine);
				if (alphaCrossing < 0 || alphaCrossing > 1)
					throw new AlphaOutsideIntervalException("alphaCrossing=" + alphaCrossing);

				boundaryLine = lp.getLineAtLowerEnvelope(alphaCrossing, stat);

				if (queryLine.compareValueTo(boundaryLine, alphaCrossing) == 0)
					return alphaCrossing;

				searchInterval[0] = alphaCrossing;
				break;

			default:
				throw new RuntimeException("Strange things did happen.");
			}

			// avoid endless loops
			iteration++;
			if (iteration >= MAXIMUM_ITERATION)
				throw new MaximumIterationException(
						"Maximum Iteration reached without result in findOptimalRangeBound!", MAXIMUM_ITERATION);
		}
	}

	/**
	 * Helper function to check if the current (query) path is optimal for the given
	 * alpha value.
	 * 
	 * @param alpha alpha value to check optimality for
	 * @param sw    stop watch for timing of running time
	 * @param stat  collector for run statistics
	 * @return
	 * @throws BoundaryLineNotOptimalException
	 */
	private boolean isOptimal(double alpha, Optional<QueryStat> stat) throws BoundaryLineNotOptimalException {
		LongLine<T> queryLine = lp.getQueryLine();
		LongLine<T> lineAtLowerEnvelope = lp.getLineAtLowerEnvelope(alpha, stat);
		stat.ifPresent(s -> s.calculatedLineAtLowerEnvelopeIn(QueryType.isOptimal));
		logger.finest("lineAtLowerEnvelope: " + lineAtLowerEnvelope.toString());

		switch (queryLine.compareValueTo(lineAtLowerEnvelope, alpha)) {
		case LINES_ARE_EQUAL:
			return true;
		case QUERY_LINE_IS_LOWER:
			throw new BoundaryLineNotOptimalException("isOptimal");
		case BOUNDARY_LINE_IS_LOWER:
			return false;
		default:
			throw new RuntimeException("Unknown compareValueTo-value");
		}
	}

	/**
	 * Searches the optimality interval for the current query path. An optional
	 * interval can be given to limit the search space in case of previous
	 * knowledge.
	 * 
	 * @param givenBounds (optional) initial bounds to limit search interval
	 * @param sw          stop watch for timing of running time
	 * @param stat        collector for run statistics
	 * @return
	 * @throws MaximumIterationException
	 * @throws InvalidIntervalException
	 * @throws BoundaryLineNotOptimalException
	 * @throws AlphaOutsideIntervalException
	 */
	public Interval findBounds(Interval givenBounds, Optional<QueryStat> stat)
			throws MaximumIterationException, BoundaryLineNotOptimalException, AlphaOutsideIntervalException {
		double low = givenBounds.getLowerBound();
		double high = givenBounds.getUpperBound();
		this.bounds[0] = low;
		this.bounds[1] = high;

		boolean lowIsOptimal = isOptimal(low, stat);
		boolean highIsOptimal = isOptimal(high, stat);

		if (lowIsOptimal && highIsOptimal) {
			logger.finest("low and high are optimal, returning initial interval");
			return givenBounds;
		}
		logger.finest("is optimal: [" + lowIsOptimal + "," + highIsOptimal + "]");

		double alphaInside;
		if (lowIsOptimal)
			alphaInside = low;
		else if (highIsOptimal)
			alphaInside = high;
		else
			alphaInside = findPointOnLowerEnvelope(givenBounds, stat);

		logger.finest("alpha_inside=" + alphaInside);
		if (alphaInside < 0)
			return null;

		if (!lowIsOptimal)
			low = findOptimalRangeBound(alphaInside, true, stat);
		if (!highIsOptimal)
			high = findOptimalRangeBound(alphaInside, false, stat);

		return new Interval(low, high);
	}

	/**
	 * See {@link #findBounds(Interval, QueryStat)}
	 * 
	 * @param sw   stop watch for timing of running time
	 * @param stat collector for run statistics
	 * @return
	 * @throws InvalidIntervalException
	 * @throws MaximumIterationException
	 * @throws BoundaryLineNotOptimalException
	 * @throws AlphaOutsideIntervalException
	 */
	public Interval findBounds(Optional<QueryStat> stat)
			throws MaximumIterationException, BoundaryLineNotOptimalException, AlphaOutsideIntervalException {
		return findBounds(new Interval(0, 1), stat);
	}
}
