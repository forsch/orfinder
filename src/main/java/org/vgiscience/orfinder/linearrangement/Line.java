package org.vgiscience.orfinder.linearrangement;

public abstract class Line<P extends Number, T> {

	public static final double EPS = 2e-12;

	protected P valueZero, valueOne;
	protected T identifier;

	public Line(P v0, P v1, T identifier) {
		this.valueZero = v0;
		this.valueOne = v1;
		this.identifier = identifier;
	}

	public final double getValue(double alpha) {
		return (1 - alpha) * valueZero.doubleValue() + alpha * valueOne.doubleValue();
	};

	public abstract int compareValueTo(Line<P, T> o, double alpha);

	public abstract double getAlphaCrossing(Line<Long, T> o);

	public final P getPrimary() {
		return valueZero;
	}

	public final P getSecondary() {
		return valueOne;
	}

	public final T getIdentifier() {
		return identifier;
	}

	public interface LineFactory<P extends Number, T> {
		public Line<P, T> generateLine(T input);
	}

	@Override
	public String toString() {
		return "[" + valueZero + " - " + valueOne + "] " + String.format("%.17s", identifier.toString())
				+ (identifier.toString().length() > 17 ? "..." : "");
	}

	@Override
	public boolean equals(Object obj) {
		return this.identifier.equals(((Line<?, ?>) obj).getIdentifier());
	}

	@Override
	public int hashCode() {
		return identifier.hashCode();
	}

	public boolean equalsTopo(Line<P, T> o) {
		if (this.valueZero.equals(o.valueZero) //
				&& this.valueOne.equals(o.valueOne))
			return true;
		return false;
	}

	public boolean dominates(Line<P, T> o) {
		if (this.valueZero.doubleValue() < o.valueZero.doubleValue() //
				&& this.valueOne.doubleValue() < o.valueOne.doubleValue())
			return true;
		return false;
	}
}
