package org.vgiscience.orfinder.linearrangement;

import java.util.Optional;

import org.vgiscience.orfinder.structures.QueryStat;

public abstract class LongLineProvider<T> extends LineProvider<Long, T> {

	@Override
	public LongLine<T> getLineAtLowerEnvelope(double alpha, Optional<QueryStat> stat) {
		return (LongLine<T>) super.getLineAtLowerEnvelope(alpha, stat);
	}

	@Override
	public LongLine<T> getLineAtLowerEnvelope(double alpha) {
		return (LongLine<T>) super.getLineAtLowerEnvelope(alpha);
	}

	@Override
	public LongLine<T> getQueryLine() {
		return (LongLine<T>) super.getQueryLine();
	}

	@Override
	protected LongLine<T> generateLineAtLowerEnvelope(double alpha) {
		return (LongLine<T>) super.generateLineAtLowerEnvelope(alpha);
	}
}
