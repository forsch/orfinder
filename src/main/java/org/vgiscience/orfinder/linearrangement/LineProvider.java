package org.vgiscience.orfinder.linearrangement;

import java.util.Optional;

import org.vgiscience.orfinder.structures.QueryStat;

public abstract class LineProvider<D extends Number, T> {

	protected T query;

	public Line<D, T> getQueryLine() {
		return generateQueryLine(query);
	}

	public Line<D, T> getLineAtLowerEnvelope(double alpha, Optional<QueryStat> stat) {
		Line<D, T> line = generateLineAtLowerEnvelope(alpha, stat);
		return line;
	}

	public Line<D, T> getLineAtLowerEnvelope(double alpha) {
		Line<D, T> line = generateLineAtLowerEnvelope(alpha, null);
		return line;
	}

	protected abstract Line<D, T> generateQueryLine(T query);

	protected abstract Line<D, T> generateLineAtLowerEnvelope(double alpha, Optional<QueryStat> stat);

	protected Line<D, T> generateLineAtLowerEnvelope(double alpha) {
		return generateLineAtLowerEnvelope(alpha, null);
	}

	public void setQuery(T query) {
		this.query = query;
	};
}
