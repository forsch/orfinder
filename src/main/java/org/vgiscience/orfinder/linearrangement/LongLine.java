package org.vgiscience.orfinder.linearrangement;

import java.lang.invoke.MethodHandles;
import java.util.logging.Logger;

public class LongLine<T> extends Line<Long, T> {
	public static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	public LongLine(Long v0, Long v1, T identifier) {
		super(v0, v1, identifier);
	}

	@Override
	public int compareValueTo(Line<Long, T> o, double alpha) {
		// if lines are topologically equal, both are equal at alpha aswell
		if (this.equalsTopo(o))
			return 0;

		// if one line dominates the other one it is lower
		if (this.dominates(o))
			return -1;
		if (o.dominates(this))
			return 1;

		// if lines have same primary or same secondary value, the line with the
		// respective higher other value is above
		if (Long.signum(this.getPrimary() - o.getPrimary()) == 0) {
			if (alpha == 0)
				return 0;
			return Long.signum(this.getSecondary() - o.getSecondary());
		}
		if (Long.signum(this.getSecondary() - o.getSecondary()) == 0) {
			if (alpha == 1)
				return 0;
			return Long.signum(this.getPrimary() - o.getPrimary());
		}

		double crossing = getAlphaCrossing(o);

		if (crossing == Double.MAX_VALUE) // lines are parallel --> line being higher at primary is above
			logger.severe("Should never happen with all cases before checked.");
//			return Long.signum(this.getPrimary() - o.getPrimary());

		if (alpha == crossing)
			return 0;

		if (crossing > alpha) // crossing is "after" query alpha --> line being higher at primary is above
			return Long.signum(this.getPrimary() - o.getPrimary());

		// if (crossing < alpha)
		// crossing is "before" query alpha --> line being higher at secondary is above
		return Long.signum(this.getSecondary() - o.getSecondary());
	}

	@Override
	/**
	 * 
	 * @source https://www.topcoder.com/community/competitive-programming/tutorials/geometry-concepts-line-intersection-and-its-applications/
	 * @param o
	 * @return
	 */
	public double getAlphaCrossing(Line<Long, T> o) {
		long p_y1 = this.getPrimary();
		long p_y2 = this.getSecondary();
		long o_y1 = o.getPrimary();
		long o_y2 = o.getSecondary();

		double det = o_y2 - o_y1 - p_y2 + p_y1;
		if (det == 0) {
			return Double.MAX_VALUE;
		} else {
			return (p_y1 - o_y1) / det;
		}
	}
}
