package org.vgiscience.orfinder.exceptions;

public class BoundaryLineNotOptimalException extends Exception {

	private static final long serialVersionUID = -6988257603563932167L;

	public BoundaryLineNotOptimalException(String message) {
		super(message);
	}
}
