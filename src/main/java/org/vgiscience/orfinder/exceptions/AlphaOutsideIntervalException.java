package org.vgiscience.orfinder.exceptions;

public class AlphaOutsideIntervalException extends Exception {

	private static final long serialVersionUID = 7620554105332103419L;

	public AlphaOutsideIntervalException(String message) {
		super(message);
	}
}
