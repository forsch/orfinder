package org.vgiscience.orfinder.exceptions;

public class MaximumIterationException extends Exception {

	private static final long serialVersionUID = -1542156983729932701L;

	public MaximumIterationException(String message, int maximumIteration) {
		super(message + ", maximum iteration = " + maximumIteration);
	}
}
