package org.vgiscience.orfinder.exceptions;

import org.vgiscience.orfinder.structures.Interval;

public class InvalidIntervalException extends Exception {

	private static final long serialVersionUID = -7592298565404156622L;

	public InvalidIntervalException(String message, Interval i) {
		super(message + " " + i);
	}
}
