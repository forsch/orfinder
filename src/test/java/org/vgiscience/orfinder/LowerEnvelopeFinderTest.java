package org.vgiscience.orfinder;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.vgiscience.orfinder.exceptions.AlphaOutsideIntervalException;
import org.vgiscience.orfinder.exceptions.BoundaryLineNotOptimalException;
import org.vgiscience.orfinder.exceptions.MaximumIterationException;
import org.vgiscience.orfinder.linearrangement.LongLine;
import org.vgiscience.orfinder.structures.Interval;
import org.vgiscience.orfinder.structures.QueryStat;

public class LowerEnvelopeFinderTest {
	private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

	@ParameterizedTest
	@MethodSource("getLineArrangements")
	public void testLowerEnvelope(ListLineProvider provider, Interval expected)
			throws MaximumIterationException, BoundaryLineNotOptimalException, AlphaOutsideIntervalException {
		LowerEnvelopeFinder<String> finder = new LowerEnvelopeFinder<>(provider);
		QueryStat stat = new QueryStat();
		Interval calculated = finder.findBounds(Optional.of(stat));
		logger.info(provider.getQueryLine().getIdentifier() + " stats: " + stat.toString());

		Assertions.assertEquals(expected, calculated);
	}

	private static Stream<Arguments> getLineArrangements() {
		ArrayList<ListLineProvider> providers = new ArrayList<>();
		ArrayList<LongLine<String>> lines;
		LongLine<String> queryLine;
		ListLineProvider provider;

		queryLine = new LongLine<>(9l, 10l, "9 - 10 - pi");
		lines = new ArrayList<>();
		lines.add(new LongLine<>(2l, 14l, "2 - 14 - p_1"));
		lines.add(new LongLine<>(5l, 12l, "5 - 12 - p_2"));
		lines.add(queryLine);
		lines.add(new LongLine<>(19l, 7l, "19 - 7 - p_3"));
		provider = new ListLineProvider(lines);
		provider.setQuery(queryLine.getIdentifier());
		providers.add(provider);

		return Stream.of(Arguments.of(providers.get(0), new Interval(0.6666666666666666, 0.7692307692307693)));
	}
}
