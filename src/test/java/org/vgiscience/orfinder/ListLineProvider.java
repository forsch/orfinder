package org.vgiscience.orfinder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.vgiscience.orfinder.linearrangement.LongLine;
import org.vgiscience.orfinder.linearrangement.LongLineProvider;
import org.vgiscience.orfinder.structures.QueryStat;

public class ListLineProvider extends LongLineProvider<String> {

	private ArrayList<LongLine<String>> lines;

	public ListLineProvider(List<LongLine<String>> lines) {
		Objects.requireNonNull(lines);
		this.lines = new ArrayList<>(lines);
	}

	@Override
	protected LongLine<String> generateQueryLine(String query) {
		String[] tokens = query.split("-");
		return new LongLine<>(Long.parseLong(tokens[0].trim()), Long.parseLong(tokens[1].trim()), query);
	}

	@Override
	protected LongLine<String> generateLineAtLowerEnvelope(double alpha, Optional<QueryStat> stat) {
		double min = 0;
		LongLine<String> best = null;
		for (LongLine<String> line : lines) {
			double val = line.getValue(alpha);
			if (best == null || val < min) {
				best = line;
				min = val;
			}
		}
		return best;
	}
}
